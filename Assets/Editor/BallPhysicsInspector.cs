﻿using UnityEditor;
using UnityEngine;

namespace Assets
{
    [CustomEditor(typeof(BallPhysics))]
    public class BallPhysicsInspector : Editor
    {   
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            var ballPhysics = target as BallPhysics;            

            if (GUILayout.Button("Add Force"))
            {
                ballPhysics.AddForce(ballPhysics.ForceToAdd);
            }
        }     
    }
}
