﻿using UnityEngine;

public class BallPhysics : MonoBehaviour
{
    public Rigidbody Rigidbody;

    public Vector3 ForceToAdd;
    
    public void AddForce(Vector3 force)
    {
        Rigidbody.AddForce(force);                
    }
}
