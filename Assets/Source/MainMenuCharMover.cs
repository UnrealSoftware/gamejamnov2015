﻿using System;
using UnityEngine;

public class MainMenuCharMover : MonoBehaviour {

    public float offset = 0;

    private float y;

    // Use this for initialization
    void Start () {
	    y = transform.position.y;
	}
	
	// Update is called once per frame
	void Update () {
	    Vector3 p = transform.position;
	    transform.position = new Vector3(p.x, y + Mathf.Sin(offset + Time.time * 5f) * 0.1f, p.z);
	}
}
