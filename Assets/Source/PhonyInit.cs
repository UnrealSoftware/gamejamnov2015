﻿using UnityEngine;
using System.Collections;

public class PhonyInit : MonoBehaviour {

    

	// Use this for initialization
	void Start () {
        AudioManager.Instance.PlayMusic(Music.Soundtrack1);
        Invoke("PlaySound",2);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void PlaySound()
    {
        AudioManager.Instance.PlaySound(Sounds.GeneralPowerUp);
        Invoke("PlaySound", 2);
    }
}
