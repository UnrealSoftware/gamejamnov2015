﻿using System.Collections;
using Assets.Source.Delegates;
using Assets.Source.enums;
using Assets.Source.QuickTimeEvent;
using Assets.Source.Root;
using Assets.Source.Root.Scripts.Roots;
using Assets.Source.PlayerController;
using UI;
using UnityEngine;
using UnityStandardAssets.Utility;
using XInputDotNetPure;

namespace Gameplay
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Instance;
        
        public TimeComponent Time;
        public ScoreBoardComponent ScoreBoard;

        public MultiFocusCamera Camera;
        
		public Throwbanana Banana { get ; set;}

		[SerializeField]
		private Throwbanana throwbananaPrefab;

        [SerializeField]
        private GameObject itemSpawnerPrefab;

        [SerializeField]
        private GameObject ballPrefab;

        [SerializeField]
        private GameObject playerTeam1Prefab;

        [SerializeField]
        private GameObject playerTeam2Prefab;

        [SerializeField]
        private GameObject npc1Prefab;

        [SerializeField]
        private GameObject npc2Prefab;

        [SerializeField]
        private RectTransform playerContainer;

        [SerializeField]
        private Vector3 ballSpawnPosition = new Vector3(0, 20, 0);

        [SerializeField]
        private Vector3 ballSpawnForce = Vector3.zero;

        [SerializeField] private GameObject quickTimeEventPrefab;

        public CharacterMovement[] players;

        public BallPhysics Ball { get; private set; }

        public int ScoreTeam1 { get; private set; }

        public int ScoreTeam2 { get; private set; }

        private bool isGameOver;

        private bool isGamePaused;

        private bool wasQuickTimeEventCreated = false;


        public void Awake()
        {
            Instance = this;
            isGameOver = false;

            players = new CharacterMovement[4];
        }

        public void Start()
        {
            if (GameRoot.Instance != null)
            {
                SetTime(GameRoot.Instance.PlayTime);
            }

            // do it first, so we can set the targte for the npcs
            Ball = Instantiate(ballPrefab).GetComponent<BallPhysics>();

            SetBall();

            Instantiate(itemSpawnerPrefab);
			Banana = Instantiate (throwbananaPrefab);
			Banana.transform.position = new Vector3(2.3f, 12.99f, 28f);
			Banana.transform.rotation = Quaternion.Euler( new Vector3(0f, 90f,0f));
            SetupPlayers();
            SetStartPositions();

            SetupCamera();
        }

        private void SetupCamera()
        {
            Camera.AddTarget(Ball.gameObject);
            foreach (var player in players)
            {
                if (player as PlayerMovement)
                {
                    Camera.AddTarget(player.gameObject);
                }
            }
        }

        public void SetBall() {
            Ball.Rigidbody.velocity = ballSpawnForce;
            Ball.transform.position = ballSpawnPosition;
            Ball.Rigidbody.AddForce(new Vector3(Random.RandomRange(10, 20), 0, Random.RandomRange(10, 20)));
        }

        private void SetStartPositions()
        {
            players[0].transform.position = new Vector3(15, 2, 10);
            players[1].transform.position = new Vector3(-10,2, 10);
            players[2].transform.position = new Vector3(10, 2, -10);
            players[3].transform.position = new Vector3(-15, 2, -10);
        }

        private void Reset()
        {
            SetStartPositions();
            Ball.Rigidbody.isKinematic = false;
            Time.Resume();
        }

        private void SetupPlayers()
        {
            int numberOfPlayers = GameRoot.Instance.PlayerNumbers;
            for (int i = 0; i < 4; i++)
            {
                int id = (i + 1);
                int team = (i % 2) + 1;
                if (id <= numberOfPlayers)
                {
                    var player = Instantiate(team == 1 ? playerTeam1Prefab :playerTeam2Prefab).GetComponent<PlayerMovement>();
                    player.transform.SetParent(playerContainer, true);
                    player.PlayerInput.PlayerIndex = id;
                    player.TeamID = team;

                    players[i] = player;
                }
                else
                {
                    var player = Instantiate(team == 1 ?npc1Prefab : npc2Prefab).GetComponent<NPCMovement>();
                    player.transform.SetParent(playerContainer, true);

                    var move = player.GetComponent<NPCMovement>();

                    move.TeamID = team;
                    move.Init();

                    players[i] = move;
                }
            }
        }

        public void SetTime(int startTime)
        {
            Time.StartTime = startTime;
        }

        public void ScoreTeam(int teamID)
        {
            if (teamID == 1)
            {
                ScoreTeam1++;
                AudioEvent.ChangeAudio(AudioGroupType.Sound, AudioFileType.Crowd);
            }
            else if (teamID == 2)
            {             
                ScoreTeam2++;
                AudioEvent.ChangeAudio(AudioGroupType.Sound, AudioFileType.Crowd);
            }

            ScoreBoard.UpdateGoalText(teamID);
            Ball.Rigidbody.isKinematic = true;
            StartCoroutine(WaitForReset());
            Time.Pause();
        }

        private IEnumerator WaitForReset() {
            yield return new WaitForSeconds(3f);
            SetBall();
            Reset();
            ScoreBoard.UpdateGoalText(0);
        }

        private void Update() {
            if (Time.Seconds == 0 && !isGameOver) {
                isGameOver = true;
                ScoreBoard.ShowGameOverText();
                Ball.Rigidbody.isKinematic = true;
                StartCoroutine(WaitAndLeaveGame());
            }

                // change this later!!
            //else if (Time.Seconds == 50 && !FindObjectOfType<QuickTimeEventHandler>())
            //{
            //   // GenerateQuickTimeEvent();
            //}
        }

        private IEnumerator WaitAndLeaveGame() {
            yield return new WaitForSeconds(5f);
            SceneEvent.ChangeScene(SceneType.MainMenu);
        }

        private void GenerateQuickTimeEvent()
        {
            if (!wasQuickTimeEventCreated)
            {
                wasQuickTimeEventCreated = true;
                Instantiate(quickTimeEventPrefab);
            }
        }

        public void PauseGame()
        {
            isGamePaused = true;
            Time.Pause();
            Ball.GetComponent<Rigidbody>().isKinematic = true;
            foreach (CharacterMovement player in players)
            {
                player.enabled = false;
            }
        }

        public void ResumeGame()
        {
            isGamePaused = false;
            Time.Resume();
            Ball.GetComponent<Rigidbody>().isKinematic = false;
            foreach (CharacterMovement player in players)
            {
                player.enabled = true;
            }
        }

        public void StunPlayer(int playerIndex)
        {
            foreach (CharacterMovement player in players)
            {
                if (player.GetComponent<PlayerInput>()!=null && player.GetComponent<PlayerInput>().PlayerIndex == playerIndex)
                {
                    player.paralyzed = true;
                    StartCoroutine("UnParalyzeQTE", player);
                }
            }
        }
    }
}