﻿using UnityEngine;
using System.Collections;

public class Throwbanana : MonoBehaviour {

	public Banana ben;
	public Banana banana; 

	// Use this for initialization
	void Start () {
		ben = Instantiate (banana);
		ben.gameObject.SetActive (false);
		StartCoroutine(ThrowBanana());
	}
	
	// Update is called once per frame
	void Update () {

	}

	public IEnumerator ThrowBanana() {
		//yield return new WaitForSeconds (Random.Range(5f, 30f));
		//yield return new WaitForSeconds (1f);
		ben.gameObject.SetActive (true);
		ben.transform.position = transform.position +Vector3.back * 5;
		ben.GetComponent<Rigidbody> ().velocity = Vector3.zero;
		ben.GetComponent<Rigidbody> ().angularDrag = 0f;
		ben.GetComponent<Rigidbody> ().rotation = Quaternion.identity;
		ben.GetComponent<Rigidbody> ().AddForce (Vector3.back * 20f, ForceMode.Impulse);
		//banana.GetComponent<Rigidbody> ().AddTorque(new Vector3 (0f,0f,0f));
		transform.Rotate (Vector3.up * 10f);
		yield return new WaitForSeconds (0.25f);
		transform.Rotate (Vector3.down * 20f);
		yield return new WaitForSeconds (0.25f);
		transform.Rotate (Vector3.up * 20f);
		yield return new WaitForSeconds (0.25f);
		transform.Rotate (Vector3.down * 20f);
		yield return new WaitForSeconds (0.25f);
		transform.Rotate (Vector3.up * 20f);
		yield return new WaitForSeconds (0.25f);
		transform.Rotate (Vector3.down * 20f);
		yield return new WaitForSeconds (0.25f);
		transform.Rotate (Vector3.up * 20f);
		yield return new WaitForSeconds (0.25f);
		transform.Rotate (Vector3.down * 20f);
		yield return new WaitForSeconds (0.25f);
		transform.Rotate (Vector3.up * 20f);
		yield return new WaitForSeconds (0.25f);
		transform.Rotate (Vector3.down * 20f);
		yield return new WaitForSeconds (0.25f);
		transform.Rotate (Vector3.up * 20f);
		yield return new WaitForSeconds (0.25f);
		transform.Rotate (Vector3.down * 10f);
		yield return new WaitForSeconds (0.25f);
		ben.Explode ();
		ben.gameObject.SetActive (false);
	}
}
