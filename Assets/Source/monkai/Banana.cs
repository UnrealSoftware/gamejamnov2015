using UnityEngine;
using System.Collections;

public class Banana : MonoBehaviour
{

	public GameObject boom;

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	public void Explode() {
		GameObject boom2 = GameObject.Instantiate (boom, transform.position, Quaternion.Euler(Vector3.one)) as GameObject;
		boom2.SetActive (true);
	}
}

