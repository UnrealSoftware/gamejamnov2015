﻿namespace Assets.Source.QuickTimeEvent
{
    public enum QuickTimeTypeEnum
    {
        ButtonA, ButtonB, ButtonX, ButtonY, DPadUp, DPadDown, DPadRight, DPadLeft
    }
}
