﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Source.PlayerController;
using Gameplay;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Source.QuickTimeEvent
{

    public class QuickTimeEventHandler:MonoBehaviour
    {
        private PlayerInput[] playerInputs;

        [SerializeField] private float timeLimit; // time in seconds
        [SerializeField] private int difficulty;
        [SerializeField] private Sprite[] sprites;
        private List<Image> spriteImages = new List<Image>();
        private List<ProgressComponent> progresses = new List<ProgressComponent>();

        [SerializeField] private RectTransform groupLayer;
        [SerializeField] private RectTransform progressLayer;
        [SerializeField] private GameObject prefabImage;
        [SerializeField] private GameObject progressComponent;

        [SerializeField] private bool resetOnWrongInput;

        private QuickTimeTypeEnum buttonTypes;
        private List<QuickTimeTypeEnum> quickTimeEvents = new List<QuickTimeTypeEnum>();
        private int[] playerProgress;

        private bool isQuickTimeEventActive = false;
        
        protected void Awake()
        {
            // grapping player inputs 
            playerInputs = FindObjectsOfType<PlayerInput>();
        }

        protected void Start()
        {
            for (var i = 0; i < difficulty; i++)
            {
                // pause game for QuickTimeEvent
                GameManager.Instance.PauseGame();
               
                QuickTimeTypeEnum newQuickTimeEvent =
                    (QuickTimeTypeEnum) UnityEngine.Random.Range(0, Enum.GetNames(typeof (QuickTimeTypeEnum)).Length);
                Debug.Log("new quick time event: " + newQuickTimeEvent);
                quickTimeEvents.Add( newQuickTimeEvent );
                GameObject newInstance = Instantiate(prefabImage);
                newInstance.GetComponent<Image>().sprite = sprites[(int)newQuickTimeEvent];
                spriteImages.Add(newInstance.GetComponent<Image>());

                newInstance.transform.SetParent(groupLayer);
            }

            for (var j = 0; j < playerInputs.Length; j++)
            {
                Debug.Log("player info name:" + playerInputs[j].name);
                GameObject newProgressInstance = Instantiate((progressComponent));
                ProgressComponent newProgress = newProgressInstance.GetComponent<ProgressComponent>();
                newProgress.Init(difficulty, getPlayerName(j), 0);
                newProgress.transform.SetParent(progressLayer, false);
                progresses.Add(newProgress);
            }

            playerProgress = new int[playerInputs.Length];

            // activate update cycle
            isQuickTimeEventActive = true;

            // start timer for deadline
            StartCoroutine(StartCountdown(timeLimit));
        }

        private void Update()
        {
            if (isQuickTimeEventActive)
            {
                CheckUserInput();
            }
        }

        protected void CheckUserInput()
        {
            // check active players -> change to the ones which are not finished yet
            for (int i = 0; i < playerInputs.Length; i++)
            {
                if (playerProgress[i] == quickTimeEvents.Count)
                {
                    // player already done
                    continue;
                }

                // check if player has input
                if (HasInput(playerInputs[i]))
                {
                    if (IsCorrectInput(playerInputs[i], quickTimeEvents[playerProgress[i]]))
                    {
                        playerProgress[i]++;
                        Debug.Log("Player " + playerInputs[i].PlayerIndex + " finished step " + playerProgress[i]);
                        // update visuals
                        UpdateVisuals(i, playerProgress[i]);

                        if (playerProgress[i] == quickTimeEvents.Count)
                        {
                            // player done
                            Debug.Log("Player " + i + " finished quick time event!");
                        }
                    }
                    else if (resetOnWrongInput)
                    {
                        Debug.Log("Player " + playerInputs[i].PlayerIndex + " failed, back to beginning: " + playerInputs[i] + " --> " + quickTimeEvents[playerProgress[i]]);
                        playerProgress[i] = 0;
                        UpdateVisuals(i, playerProgress[i]);
                    }
                }
            }
        }

        protected bool HasInput(PlayerInput playerInput)
        {
            return (playerInput.ButtonADown || playerInput.ButtonBDown || playerInput.ButtonXDown || playerInput.ButtonYDown ||
                    playerInput.DPadUpDown || playerInput.DPadDownDown || playerInput.DPadLeftDown || playerInput.DPadRightDown);
        }

        protected bool IsCorrectInput(PlayerInput playerInput, QuickTimeTypeEnum quickTimeEvent)
        {
            switch (quickTimeEvent)
            {
                case QuickTimeTypeEnum.ButtonA:
                    return playerInput.ButtonADown;
                case QuickTimeTypeEnum.ButtonB:
                    return playerInput.ButtonBDown;
                case QuickTimeTypeEnum.ButtonX:
                    return playerInput.ButtonXDown;
                case QuickTimeTypeEnum.ButtonY:
                    return playerInput.ButtonYDown;
                case QuickTimeTypeEnum.DPadUp:
                    return playerInput.DPadUpDown;
                case QuickTimeTypeEnum.DPadDown:
                    return playerInput.DPadDownDown;
                case QuickTimeTypeEnum.DPadRight:
                    return playerInput.DPadRightDown;
                case QuickTimeTypeEnum.DPadLeft:
                    return playerInput.DPadLeftDown;
                default:
                    return false;
            }
        }

        private String getPlayerName(int inputCount)
        {
            switch (inputCount)
            {
                case 0:
                    return "P1";
                case 1:
                    return "P2";
                case 2:
                    return "P3";
                case 3:
                    return "P4";
                default:
                    return "";
            }
        }

        protected void UpdateVisuals(int playerIndex, int progress)
        {
       
            /*for (var i = 0; i < quickTimeEvents.Count; i++)
            {
               Color newColor = spriteImages[i].color; 
               newColor.a = i < progress ? 0.5f : 1f;
               spriteImages[i].color = newColor;
            }*/

            progresses[playerIndex].SetProgress(progress);
        }

        protected IEnumerator StartCountdown(float duration)
        {
            yield return new WaitForSeconds(duration);

            isQuickTimeEventActive = false;
            Debug.Log("Event over!");

            // resume game in GameManager
            GameManager.Instance.ResumeGame();

            // evaluate player progress
            for (var i = 0; i < playerProgress.Length; i++)
            {
                if (playerProgress[i] == quickTimeEvents.Count)
                {
                    Debug.Log("Player " + playerInputs[i].PlayerIndex + " pleased the God");
                    // dispatch success signal for this player
                }
                else
                {
                    Debug.Log("Player " + playerInputs[i].PlayerIndex + " failed!");
                    // dispatch fail signal for this player
                    //GameManager.Instance.StunPlayer(playerInputs[i].PlayerIndex);
                    // slow player character down - set him on fire
                }
            }

            Destroy(this.gameObject);
        }
    }
}
