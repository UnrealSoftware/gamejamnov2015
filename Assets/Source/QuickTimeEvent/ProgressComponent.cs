﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Source.QuickTimeEvent
{
    public class ProgressComponent:MonoBehaviour
    {
        private int maxSteps;
        private int currentStep;

        private GameObject visuals;

        private Slider sliderComponent;

        [SerializeField]
        private Image sliderFillComponent;

        [SerializeField]
        private Text textComponent;

        [SerializeField]
        private Image imageComponent;

        public Color Team1Color = Color.red;
        public Color Team2Color = Color.blue;
        public Color CompleteColor = Color.green;

        public void Awake()
        {
            sliderComponent = GetComponent<Slider>();
            currentStep = 0;
        }

        public void Init(int maxSteps, String playerName, int team)
        {
            this.maxSteps = maxSteps;
            sliderComponent.maxValue = maxSteps;
            sliderComponent.value = currentStep;

            // set Player name like "P1" "P2" etc.
            textComponent.text = playerName;

            // set Logo of the team
            if (team == 0)
            {
                imageComponent.overrideSprite = Resources.Load<Sprite>("Images/team1");
                sliderFillComponent.color = Team1Color;
            }
            else
            {
                imageComponent.overrideSprite = Resources.Load<Sprite>("Images/team2");
                sliderFillComponent.color = Team2Color;
            }
          
        }

        public void SetProgress(int step)
        {
            currentStep = step;
            if (currentStep == maxSteps)
            {
                // special graphic effect
                ShowCompleteGraphic();
            }

            sliderComponent.value = step;
        }

        protected void ShowCompleteGraphic()
        {
            sliderFillComponent.color = CompleteColor;
        }
    }
}
