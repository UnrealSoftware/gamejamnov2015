using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MultiFocusCamera : MonoBehaviour
{
	protected enum Alignment { Center, Left, Right,Top, Bottom, TopLeft, TopRight, BottomLeft, BottomRight };

	[SerializeField] protected List<GameObject> targets = new List<GameObject>();
	[SerializeField] protected Alignment alignment;
	[SerializeField] protected Vector3 offset;
	[SerializeField] protected float minDistance = 5f;
	[SerializeField] protected float maxDistance = 20f;
	[SerializeField] protected float speed = 1f;
	[SerializeField] protected float camRayLength = 50f;

	int obstacleMask; 
	int playerLayer; 

	protected virtual void Start() 
	{
		SetAlignment(alignment);
		obstacleMask = LayerMask.GetMask ("SolidEnvironment");
		playerLayer = LayerMask.GetMask ("player");
	}

	protected virtual void LateUpdate() 
	{	
		GameObject[] targets = this.targets.ToArray();
		
		if(targets.Length <= 0)
			return;
		
		Vector3 position = CenterOf(targets);
		Vector3 velocity = VelocityOf(targets);
		Vector3 direction = velocity.normalized;

		Quaternion rotation = Quaternion.LookRotation(position - transform.position);

		float distance = DistanceTo(targets,position);
		float speed = (this.speed + velocity.magnitude) * Time.deltaTime;

		if (areAllObjectsVisible(targets) & (offset.x < maxDistance))
		{
			offset.x += 1;
			offset.y += 1;
			offset.z += 1;
		}

		distance = Mathf.Clamp(distance,minDistance,maxDistance);

		position.x += direction.x + offset.x;
		position.z -= distance + offset.z;
		position.y += direction.y + offset.y;

		transform.rotation = Quaternion.Slerp(transform.rotation,rotation,speed);
		transform.position = Vector3.Lerp(transform.position,position,speed);
	}
	
	private Vector3 CenterOf(GameObject[] targets)
	{
		Vector3 difference = Vector3.zero;
		
		foreach(GameObject target in targets)
		{
			difference += target.transform.position; 
		}
		
		return difference / targets.Length;
	}

	private bool areAllObjectsVisible(GameObject[] targets)
	{
		bool targetConcealed = false;
		Ray camRay;
		foreach (GameObject target in targets)
		{
			camRay = Camera.main.ScreenPointToRay (target.transform.position);
			RaycastHit obstacleHit;
            var layerMask = ~((1 << playerLayer));
            if (Physics.Raycast (camRay, out obstacleHit, camRayLength, layerMask))
			{
				targetConcealed = true;
				break;
			}
		}
		return targetConcealed;
	}

	private float DistanceTo(GameObject[] targets,Vector3 position)
	{
		float distance = 0;
		foreach(GameObject target in targets)
		{
			distance += Vector3.Distance(position,target.transform.position);
		}
		
		return distance;
	}
	
	Vector3 VelocityOf(GameObject[] targets)
	{
		Vector3 velocity = Vector3.zero;
		int count = 0;
		
		foreach(GameObject target in targets)
		{
			Rigidbody rigidbody = target.GetComponent<Rigidbody>();

			if(rigidbody)
			{
				velocity += rigidbody.velocity;
				count++;
			}
		}
		return velocity / Mathf.Max(count,1);
	}
	
	public virtual void AddTarget(GameObject target)
	{
		targets.Add(target);
	}
	
	public virtual void RemoveTarget(GameObject target)
	{
		targets.Remove(target);
	}

	public virtual void RemoveAllTargets()
	{
		targets = new List<GameObject>();
	}

	protected void SetAlignment(Alignment alignment)
	{
		switch(alignment)
		{
		case Alignment.Center:
			GetComponent<Camera>().rect = new Rect(0,0,1,1);
			break;
		case Alignment.Left:
			GetComponent<Camera>().rect = new Rect(0,0,0.5f,1);
			break;
		case Alignment.Right:
			GetComponent<Camera>().rect = new Rect(0.5f,0,1,1);
			break;
		case Alignment.Top:
			GetComponent<Camera>().rect = new Rect(0,0.5f,1,1);
			break;
		case Alignment.Bottom:
			GetComponent<Camera>().rect = new Rect(0,0,1,0.5f);
			break;
		case Alignment.TopLeft:
			GetComponent<Camera>().rect = new Rect(0,0.5f,0.5f,1);
			break;
		case Alignment.TopRight:
			GetComponent<Camera>().rect = new Rect(0.5f,0.5f,1,1);
			break;
		case Alignment.BottomLeft:
			GetComponent<Camera>().rect = new Rect(0,0,0.5f,0.5f);
			break;
		case Alignment.BottomRight:
			GetComponent<Camera>().rect = new Rect(0.5f,0,1,0.5f);
			break;
		}
	}
}


