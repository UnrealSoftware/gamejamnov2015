﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum Sounds{
    BumpWithBallSound,
    BumpWithFriendSound,
    BumpWithOpponentSound,
    GeneralPowerUp,
    ScoreSound,
    DieSound,
    GetHurtSound,
    HurtOtherSound
}

public enum Music{
    Soundtrack1,
    Soundtrack2,
    OpeningSong,
    WinSong,
    LoseSong
}

[System.Serializable]
public struct SoundAttribution
{
    public Sounds soundType;
    public AudioClip soundFile;
}

[System.Serializable]
public struct MusicAttribution
{
   public Music musicType;
   public  AudioClip soundFile;
}
[RequireComponent(typeof(AudioSource))]
public class AudioManager : MonoBehaviour {

    public SoundAttribution[] Sounds;
    public MusicAttribution[] Music;
    public AudioSource AudioSrc;

    private static AudioManager instance;
    public static AudioManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<AudioManager>();
                if (instance == null)
                {
                    Debug.LogError("Put an audiomanager into the scene and initialize it properly.");
                }
            }
            return instance;
        }
    }

    
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void PlayMusic(Music musicType)
    {
        for (int i = 0; i < Music.Length; i++)
        {
            if (Music[i].musicType == musicType)
            {
                AudioSrc.clip = Music[i].soundFile;
                AudioSrc.Play();
            }
        }
    }

    public void PlaySound(Sounds soundType)
    {
        for (int i = 0; i < Sounds.Length; i++)
        {
            if (Sounds[i].soundType == soundType)
            {
                AudioSrc.PlayOneShot(Sounds[i].soundFile);
            }
        }
    }
}
