﻿using UnityEngine;
using System.Collections;

public class UpAndDown : MonoBehaviour {

    public int dist = 1;

    private float initHeight;
    private int dir = 1;
    private float speed;
	// Use this for initialization
	void Start () {
        initHeight = transform.position.y;
        speed = Random.RandomRange(4, 6);
	}
	
	// Update is called once per frame
	void Update () {
        this.transform.position += Vector3.up * speed * dir * Time.deltaTime;
        if (transform.position.y > initHeight+dist)
        {
            dir = -1;
        }
        if (transform.position.y < initHeight)
        {
            dir = 1;
            speed = Random.RandomRange(4,6);
        }
	}
}
