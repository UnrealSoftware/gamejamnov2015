﻿using UnityEngine;
using Gameplay;
using System.Collections;

namespace Gameplay
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(CapsuleCollider))]
    public abstract class CharacterMovement : MonoBehaviour
    {       
        public int TeamID { get; set; }

        public float Pitch = 0.05f;
        public float HighPitch = 0.4f;

        private Vector3 lastMovementDirection = Vector3.one;

        private bool isNpc;

        [SerializeField]
        private float rotateSpeed = 400.0f;
        [SerializeField]
        private float moveSpeed = 600.0f;
        [SerializeField]
        private float jumpSpeed = 1500.0f;
        [SerializeField]
        private float damping = 10f;

        [SerializeField]
        private int shootPower = 500;

        [SerializeField]
        private int shootPower2 = 100;

        private Rigidbody rigidbody;
        private CapsuleCollider collider;

        private Vector3 movement;
        
        private float force;
        private Vector3 rotationForwardDirection;
        
        protected BallPhysics ball;

        protected CharacterMovement Opponent { get; set; }

        private Transform meshTransform;

        private int collisionGroundCount;

        //Check for grounded
        public bool IsGrounded
        {
            get
            {
                return collisionGroundCount > 0;
            }
        }

public abstract Vector3 MovementDirection { get; }

        public abstract bool Shot { get; }

        public abstract bool Shot2 { get; }

        public abstract bool Punch { get; }        

        public abstract bool Jump { get; }

        public Rigidbody Rigidbody
        {
            get { return rigidbody; }
        }

        public void AddForce(Vector3 force)
        {
            rigidbody.AddForce(force);
        }

        #region monobehaviour
        protected virtual void Awake()
        {
            rigidbody = GetComponent<Rigidbody>();
            collider = GetComponent<CapsuleCollider>();
            MeshRenderer renderer = GetComponentInChildren<MeshRenderer>();
            if (renderer != null) {
                meshTransform = renderer.transform;
            }
            isNpc = GetComponentInChildren<NPCMovement>() != null;
        }

        protected virtual void Start()
        {   
        }

        public bool paralyzed;

        protected void FixedUpdate()
        {
            if (paralyzed)
            {
                return;
            }

            float speed = Mathf.Clamp01(Mathf.Abs(MovementDirection.x) + Mathf.Abs(MovementDirection.z));
            Vector3 direction = transform.TransformDirection(MovementDirection);

            //if (!isNpc) {
                if (MovementDirection != Vector3.zero) {
                    lastMovementDirection = MovementDirection;
                }
                Vector3 targetDir = -lastMovementDirection;
                float step = 0.1f*Time.deltaTime;
                Vector3 newDir = Vector3.RotateTowards(transform.up, targetDir, step, 5.0F);
                Debug.DrawRay(meshTransform.position, newDir, Color.red);
                meshTransform.rotation = Quaternion.LookRotation(newDir);
            //}

            if (IsGrounded)
            {
                UpdateOnGround(speed, direction);                
            }           

            if (Shot)
            {
                ShotBall(Pitch, shootPower);
            }

            if (Shot2)
            {
                ShotBall(HighPitch, shootPower2);
            }
 
            if (Punch) // && (Time.time - lastPunch) > 2)
            {
                StartPunch();
            }
 
            // [R] Reset ball position
            if (Input.GetKeyDown(KeyCode.R)) {
                GameManager.Instance.SetBall();
            }    
        }

        private float lastPunch;

        protected void StartPunch()
        {
            lastPunch = Time.time;

            var players = GameManager.Instance.players;

            foreach (var player in players)
            {
                if (player.TeamID == TeamID)
                {
                    continue;
                }

                var distance = Vector3.Distance(player.transform.position, transform.position);

                if (distance < 3f)
                {
                    var shootForce = Vector3.RotateTowards(
                        MovementDirection,
                        Vector3.up,

                        (2 * Mathf.PI) * .01f,
                        0f);

                    player.paralyzed = true;
                    player.AddForce(shootForce * shootPower * 0.3f);
                    //ball.GetComponent<ParticleSystem>().Play();
                    StartCoroutine(player.UnParalyze());
                }
            }                     
        }

        protected IEnumerator UnParalyze()
        {
            yield return new WaitForSeconds(0.5f);
            paralyzed = false;
        }

        public IEnumerator UnParalyzeQTE()
        {
            Debug.Log("wait 3 seconds before UnParalyzing");
            yield return new WaitForSeconds(3f);
            Debug.Log("Time to unparalyze");
            paralyzed = false;
        }

        protected virtual void AfterPunch()
        {            
        }

        protected virtual void Update()
        {           
        }

        #endregion

        protected void ShotBall(float pitch, int shootingPower)
        {
            var ball = GameManager.Instance.Ball;

            var distance = Vector3.Distance(ball.transform.position, transform.position);

            if (distance < 3f)
            {
                var shootForce = Vector3.RotateTowards(
                    MovementDirection,
                    Vector3.up,
                                        
                    (2 * Mathf.PI) * pitch,
                    0f);

                ball.AddForce(shootForce * shootingPower);
                ball.GetComponent<ParticleSystem>().Play();
            }
        }

        protected virtual void UpdateInAir()
        {
            var velocity = rigidbody.velocity;
            if (velocity.y > 0)
            {
                velocity.y = 0;
                rigidbody.transform.RotateAround(transform.position, rotationForwardDirection, velocity.magnitude);
            }
        }

        protected virtual void UpdateOnGround(float speed, Vector3 direction)
        {
            force = Mathf.Lerp(force, speed, Time.deltaTime * damping);

            speed = force * Time.deltaTime * moveSpeed;
            direction *= speed;
            direction.y = rigidbody.velocity.y;
            
            rigidbody.velocity = direction;

            if (Jump)
            {            
                rigidbody.AddForce(transform.up * jumpSpeed);
            }         
        }

        protected virtual void OnTriggerEnter(Collider collider)
        {
            if (Opponent == null)
            {
                Opponent = collider.gameObject.GetComponent<CharacterMovement>();
            }            
        }

        protected virtual void OnTriggerExit(Collider collider)
        {
            if (Opponent != null)
            {
                Opponent = null;
            }
        }
        protected virtual void OnCollisionEnter(Collision collision)
        {
            collisionGroundCount++;
        }

        protected virtual void OnCollisionExit(Collision collision)
        {
            collisionGroundCount--;
        }
    }
}
