﻿using System;
using Assets.Source.items;
using Assets.Source.PlayerController;
using UnityEngine;

namespace Gameplay
{
    [RequireComponent(typeof(PlayerInput))]
    public class PlayerMovement : CharacterMovement
    {

        [SerializeField]
        private Color playerColor;

        [SerializeField]
        private float shakeDuration = 0.2f;
        
        private PlayerInput input;
        
        private bool inJump;

        private TextMesh text;

        public PlayerInput PlayerInput
        {
            get
            {
                return input;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            input = GetComponent<PlayerInput>();
            text = GetComponentInChildren<TextMesh>();
        }

        protected override void Start()
        {
            base.Start();
            transform.name = "Player: " + input.PlayerIndex;
            text.text = input.PlayerIndex.ToString();
        }

        protected override void OnCollisionEnter(Collision collision)
        {
            base.OnCollisionEnter(collision);

            ShakeController(collision);

            CharacterMovement character = collision.transform.GetComponent<CharacterMovement>();
            if (character && Rigidbody)
            {
                Vector3 velocity = -Rigidbody.velocity/2;
                Rigidbody.velocity = velocity;//own velocity
                character.Rigidbody.velocity -= velocity; //other velocity
            }

            if (IsGrounded)
            {
                inJump = false;
            }
        }
        
        protected override void AfterPunch()
        {
            ShakeController();

            if (Opponent)
            {
                if (Opponent is PlayerMovement)
                {
                    (Opponent as PlayerMovement).ShakeController();
                }
            }

            base.AfterPunch();
        }

        public void CollectItem(ItemType type)
        {
            Debug.Log("Collected item of type " + type.ToString());
			StartCoroutine( GameManager.Instance.Banana.ThrowBanana());
        }

        public void ShakeController(Collision collision = null)
        {
            if (collision != null)
            {
                var direction = (collision.transform.position - transform.position).normalized;
                input.StartVibration(new Vector2(direction.x, direction.z), shakeDuration);
            }            
            else
            {
                var direction = transform.position.normalized;
                input.StartVibration(new Vector2(direction.x, direction.z), shakeDuration);
            }
            
        }

        public override Vector3 MovementDirection
        {
            get { return new Vector3(input.LeftHorizontal, 0, input.LeftVertical); }
        }

        #region actions

        public override bool Jump
        {
            get
            {
                bool press = input.ButtonA;
                if (press && IsGrounded && !inJump)
                {
                    inJump = press;
                    return press;
                }
                return false;
            }
        }

        public override bool Shot
        {
            get
            {
                return input.ButtonB;
            }
        }

        public override bool Punch
        {
            get
            {
                return input.ButtonX;
            }
        }

        public override bool Shot2
        {
            get
            {
                return input.ButtonY;
            }
        }        

        #endregion
    }
}
