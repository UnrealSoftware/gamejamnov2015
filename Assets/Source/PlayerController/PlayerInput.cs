﻿using System.Collections;
using UnityEngine;
#if UNITY_STANDALONE_WIN
using XInputDotNetPure;
#endif

namespace Assets.Source.PlayerController
{
    public class PlayerInput : MonoBehaviour
    {
		#if UNITY_STANDALONE_WIN
        [SerializeField]
        private PlayerIndex playerIndex;
		private GamePadState state;
        private GamePadState prevState;
		#endif

		#if UNITY_STANDALONE_OSX
		[SerializeField]
		public int playerIndex = 0;
		#endif

        protected void Awake()
        {

        }

        protected void Start()
        {
			#if UNITY_STANDALONE_WIN
            state = GamePad.GetState(playerIndex);
			#endif

        }

        protected void Update()
        {
#if UNITY_STANDALONE_WIN
            prevState = state;
            state = GamePad.GetState(playerIndex);
			#endif
        }

        #region Button A
        public bool ButtonADown
        {
            get
            {
				#if UNITY_STANDALONE_OSX 
				switch (playerIndex){
				case 1 : return Input.GetKeyDown(KeyCode.K);
				case 2 : return Input.GetKeyDown(KeyCode.G);
				default : return Input.GetKeyDown(KeyCode.K);
				}
				#endif

				#if UNITY_STANDALONE_WIN 
				return prevState.Buttons.A == ButtonState.Released && state.Buttons.A == ButtonState.Pressed || Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Space);
				#endif
            }
        }

        public bool ButtonA
        {
            get
            {
				#if UNITY_STANDALONE_OSX 
				switch (playerIndex){
				case 1 : return Input.GetKeyUp(KeyCode.K);
				case 2 : return Input.GetKeyUp(KeyCode.G);
				default : return Input.GetKeyUp(KeyCode.K);
				}
				#endif

				#if UNITY_STANDALONE_WIN 
                return state.Buttons.A == ButtonState.Pressed || Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.Space);
				#endif
            }
        }
        #endregion

        #region Button B
        public bool ButtonBDown
        {
            get
            {
				#if UNITY_STANDALONE_OSX 
				switch (playerIndex){
				case 1 : return Input.GetKeyDown(KeyCode.L);
				case 2 : return Input.GetKeyDown(KeyCode.H);
				default : return Input.GetKeyDown(KeyCode.L);
				}
				#endif

				#if UNITY_STANDALONE_WIN 
                return prevState.Buttons.B == ButtonState.Released && state.Buttons.B == ButtonState.Pressed || Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.F);
				#endif
            }
        }

        public bool ButtonB
        {
            get
            {
				#if UNITY_STANDALONE_OSX 
				switch (playerIndex){
				case 1 : return Input.GetKeyUp(KeyCode.L);
				case 2 : return Input.GetKeyUp(KeyCode.H);
				default : return Input.GetKeyUp(KeyCode.L);
				}
				#endif

				#if UNITY_STANDALONE_WIN 
                return state.Buttons.B == ButtonState.Pressed || Input.GetKeyDown(KeyCode.Escape) || Input.GetKey(KeyCode.F);
				#endif
            }
        }
        #endregion

        #region Button X
        public bool ButtonXDown
        {
            get
            {
				#if UNITY_STANDALONE_OSX 
				switch (playerIndex){
				case 1 : return Input.GetKeyDown(KeyCode.J);
				case 2 : return Input.GetKeyDown(KeyCode.F);
				default : return Input.GetKeyDown(KeyCode.J);
				}
				#endif

				#if UNITY_STANDALONE_WIN 
                return prevState.Buttons.X == ButtonState.Released && state.Buttons.X == ButtonState.Pressed || Input.GetKeyDown(KeyCode.LeftShift);
				#endif
            }
        }

        public bool ButtonX
        {
            get
            {
				#if UNITY_STANDALONE_OSX 
				switch (playerIndex){
				case 1 : return Input.GetKeyUp(KeyCode.J);
				case 2 : return Input.GetKeyUp(KeyCode.F);
				default : return Input.GetKeyUp(KeyCode.J);
				}
				#endif

				#if UNITY_STANDALONE_WIN 
                return state.Buttons.X == ButtonState.Pressed || Input.GetKey(KeyCode.LeftShift);
				#endif
            }
        }
        #endregion

        #region Button Y
        public bool ButtonYDown
        {
            get
            {
				#if UNITY_STANDALONE_OSX 
				switch (playerIndex){
				case 1 : return Input.GetKeyDown(KeyCode.I);
				case 2 : return Input.GetKeyDown(KeyCode.T);
				default : return Input.GetKeyDown(KeyCode.I);
				}
				#endif

				#if UNITY_STANDALONE_WIN 
                return prevState.Buttons.Y == ButtonState.Released && state.Buttons.Y == ButtonState.Pressed || Input.GetKeyDown(KeyCode.R);
				#endif
            }
        }

        public bool ButtonY
        {
            get
            {
				#if UNITY_STANDALONE_OSX 
				switch (playerIndex){
				case 1 : return Input.GetKeyUp(KeyCode.I);
				case 2 : return Input.GetKeyUp(KeyCode.T);
				default : return Input.GetKeyUp(KeyCode.I);
				}
				#endif

				#if UNITY_STANDALONE_WIN 
                return state.Buttons.Y == ButtonState.Pressed ||  Input.GetKey(KeyCode.R);
				#endif
            }
        }
        #endregion

        #region DPad Up
        public bool DPadUpDown
        {
            get
            {
				#if UNITY_STANDALONE_OSX 
				switch (playerIndex){
				case 1 : return Input.GetKeyDown(KeyCode.UpArrow);
				case 2 : return Input.GetKeyDown(KeyCode.W);
				default : return Input.GetKeyDown(KeyCode.UpArrow);
				}
				#endif

				#if UNITY_STANDALONE_WIN 
                return prevState.DPad.Up == ButtonState.Released && state.DPad.Up == ButtonState.Pressed || Input.GetKeyDown(KeyCode.UpArrow);
				#endif
            }
        }

        public bool DPadUp
        {
            get
			{
				#if UNITY_STANDALONE_OSX 
				switch (playerIndex){
				case 1 : return Input.GetKeyUp(KeyCode.UpArrow);
				case 2 : return Input.GetKeyUp(KeyCode.W);
				default : return Input.GetKeyUp(KeyCode.UpArrow);
				}
				#endif

				#if UNITY_STANDALONE_WIN 
                return state.DPad.Up == ButtonState.Pressed || Input.GetKey(KeyCode.UpArrow);
				#endif
            }
        }
        #endregion

        #region DPad Down
        public bool DPadDownDown
        {
            get
			{
				#if UNITY_STANDALONE_OSX 
				switch (playerIndex){
				case 1 : return Input.GetKeyDown(KeyCode.DownArrow);
				case 2 : return Input.GetKeyDown(KeyCode.S);
				default : return Input.GetKeyDown(KeyCode.DownArrow);
				}
				#endif

				#if UNITY_STANDALONE_WIN 
                return prevState.DPad.Down == ButtonState.Released && state.DPad.Down == ButtonState.Pressed || Input.GetKeyDown(KeyCode.DownArrow);
				#endif
            }
        }

        public bool DPadDown
        {
            get
            {
				#if UNITY_STANDALONE_OSX 
				switch (playerIndex){
				case 1 : return Input.GetKeyUp(KeyCode.DownArrow);
				case 2 : return Input.GetKeyUp(KeyCode.S);
				default : return Input.GetKeyUp(KeyCode.DownArrow);
				}
				#endif

				#if UNITY_STANDALONE_WIN 
                return state.DPad.Down == ButtonState.Pressed || Input.GetKey(KeyCode.DownArrow);
				#endif
            }
        }
        #endregion

        #region DPad Right
        public bool DPadRightDown
        {
            get
			{
				#if UNITY_STANDALONE_OSX 
				switch (playerIndex){
				case 1 : return Input.GetKeyDown(KeyCode.RightArrow);
				case 2 : return Input.GetKeyDown(KeyCode.D);
				default : return Input.GetKeyDown(KeyCode.RightArrow);
				}
				#endif

				#if UNITY_STANDALONE_WIN 
                return prevState.DPad.Right == ButtonState.Released && state.DPad.Right == ButtonState.Pressed || Input.GetKeyDown(KeyCode.RightArrow);
				#endif
            }
        }

        public bool DPadRight
        {
            get
            {
				#if UNITY_STANDALONE_OSX 
				switch (playerIndex){
				case 1 : return Input.GetKeyUp(KeyCode.RightArrow);
				case 2 : return Input.GetKeyUp(KeyCode.D);
				default : return Input.GetKeyUp(KeyCode.RightArrow);
				}
				#endif

				#if UNITY_STANDALONE_WIN 
                return state.DPad.Right == ButtonState.Pressed || Input.GetKey(KeyCode.RightArrow);
				#endif
            }
        }
        #endregion

        #region DPad Up
        public bool DPadLeftDown
        {
            get
            {
				#if UNITY_STANDALONE_OSX 
				switch (playerIndex){
				case 1 : return Input.GetKeyDown(KeyCode.LeftArrow);
				case 2 : return Input.GetKeyDown(KeyCode.A);
				default : return Input.GetKeyDown(KeyCode.LeftArrow);
				}
				#endif

				#if UNITY_STANDALONE_WIN 
                return prevState.DPad.Left == ButtonState.Released && state.DPad.Left == ButtonState.Pressed || Input.GetKeyDown(KeyCode.LeftArrow);
				#endif
            }
        }

        public bool DPadLeft
        {
            get
            {
				#if UNITY_STANDALONE_OSX 
				switch (playerIndex){
				case 1 : return Input.GetKeyUp(KeyCode.LeftArrow);
				case 2 : return Input.GetKeyUp(KeyCode.A);
				default : return Input.GetKeyUp(KeyCode.LeftArrow);
				}
				#endif

				#if UNITY_STANDALONE_WIN 
                return state.DPad.Left == ButtonState.Pressed || Input.GetKey(KeyCode.LeftArrow);
				#endif
            }
        }
        #endregion

        #region ThumbSticks
        public float LeftVertical
        {
            get
            {
				#if UNITY_STANDALONE_OSX 
				switch (playerIndex) {
				case 1 : return Input.GetAxis("Vertical");
				case 2 : return Input.GetAxis("Vertical2");
				default : return Input.GetAxis("Vertical");
				}
				#endif

				#if UNITY_STANDALONE_WIN 
                return state.ThumbSticks.Left.Y;
				#endif
            }
        }

        public float LeftHorizontal
        {
            get
            {
				#if UNITY_STANDALONE_OSX 
				switch (playerIndex) {
				case 1 : return Input.GetAxis("Horizontal");
				case 2 : return Input.GetAxis("Horizontal2");
				default : return Input.GetAxis("Horizontal");
				}
				#endif

				#if UNITY_STANDALONE_WIN 
                return state.ThumbSticks.Left.X;
				#endif
            }
        }
        #endregion

        public void StartVibration(Vector2 direction, float duration)
        {
			#if UNITY_STANDALONE_WIN 
            StartCoroutine(Vibration(direction, duration));
			#endif
        }

        private IEnumerator Vibration(Vector2 direction, float duration)
        {

			#if UNITY_STANDALONE_OSX
			yield return new WaitForSeconds(0f);
			#endif

			#if UNITY_STANDALONE_WIN 
            GamePad.SetVibration(playerIndex, direction.x, direction.y);
            yield return new WaitForSeconds(duration);
            GamePad.SetVibration(playerIndex, 0, 0);
			#endif
        }

		#if UNITY_STANDALONE_WIN 
        public void StopVibration()
        {
            StopCoroutine("Vibration");
        }
		#endif

        public int PlayerIndex
        {
            get
            {
#if UNITY_STANDALONE_OSX
			return playerIndex;
#endif

#if UNITY_STANDALONE_WIN
                return (int) playerIndex + 1;
#endif
                return (int)playerIndex + 1;
            }
            set
            {
#if UNITY_STANDALONE_OSX
			playerIndex = value;
#endif

#if UNITY_STANDALONE_WIN
                playerIndex = (PlayerIndex) (value - 1);
                state = GamePad.GetState(playerIndex);
#endif
            }
        }

        public bool IsActive
        {
            get
            {
#if UNITY_STANDALONE_WIN
                return state.IsConnected;
#endif
                return false;
            }
        }
    }
}
