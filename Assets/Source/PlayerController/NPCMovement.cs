﻿using System;
using UnityEngine;
using Assets.Goal;

namespace Gameplay
{

    public enum Behaviours
    {
        Attack,
        MidAttack,
        MidDefend,
        Defend
    } 

    public enum Decisions
    {
        Idle,
        ShootGoal,
        MoveIntoAtkArea,
        MoveIntoDefArea,
        ShootBallAway,
        MoveToBall,
        MoveToBallConditional,
        GetInBetweenBallAndGoal,
        RepositionBehindBall,
        GoHomeConditional,
        GoHome
    };

    public class NPCMovement : CharacterMovement
    {
        [SerializeField]
        private Transform gameBall;
        [SerializeField]
        private Transform bottomAtkCorner;
        [SerializeField]
        private Transform bottomDefCorner;
        [SerializeField]
        private Transform topAtkCorner;
        [SerializeField]
        private Transform topDefCorner;

        [SerializeField]
        private Transform enemyGoalT;
        private Vector3 enemyGoal
        {
            get {
                Vector3 pos = enemyGoalT.position;
                pos.y = 0;
                return pos;
            }
        }
        private Vector3 lastTarget;
        private int counter = 0;

        [SerializeField]
        private Transform myGoalT;
        private Vector3 myGoal
        {
            get
            {
                Vector3 pos = myGoalT.position;
                pos.y = 0;
                return pos;
            }
        }
        public Behaviours startingBehaviour;

        private Decisions currentDecision;
        private Behaviours currentBehaviour;

        public void Init()
        {
            foreach(GoalComponent goal in GameObject.FindObjectsOfType <GoalComponent>()){
                if(goal.TeamID == TeamID)
                {
                    myGoalT = goal.transform;
                }
                else
                {
                    enemyGoalT = goal.transform;
                }
            }
            ball = GameManager.Instance.Ball;
            gameBall = ball.transform;
            foreach (GameObject mapCorner in GameObject.FindGameObjectsWithTag("corner"))
            {
                if (mapCorner.transform.position.z > gameBall.transform.position.z)
                {
                    //TopCorners
                    if(GetMyDistance(myGoal, mapCorner.transform.position) < GetMyDistance(enemyGoal, mapCorner.transform.position))
                    {
                        //defCorner
                        topDefCorner = mapCorner.transform;
                    }
                    else
                    {
                        topAtkCorner = mapCorner.transform;
                    }
                }
                else
                {
                    //BottomCorners
                    if (GetMyDistance(myGoal, mapCorner.transform.position) < GetMyDistance(enemyGoal, mapCorner.transform.position))
                    {
                        //defCorner
                        bottomDefCorner = mapCorner.transform;
                    }
                    else
                    {
                        bottomAtkCorner = mapCorner.transform;
                    }
                }
            }
            startingBehaviour = Behaviours.MidDefend;
            foreach(NPCMovement npc in GameObject.FindObjectsOfType<NPCMovement>())
            {
                if(npc!=this && npc.TeamID == TeamID)
                {
                    startingBehaviour = Behaviours.MidAttack;
                }
            }
            
        }

        public override bool Jump
        {
            get
            {
                return false;
            }
        }

        public override Vector3 MovementDirection
        {
            get
            {
                Vector3 direction = TargetDirection;
                return direction;
            }
        }

        private Vector3 TargetDirection
        {
            get
            {
                Vector3 target = GetNextTarget;
                Vector3 direction = (target - transform.position).normalized;
                return direction;
            }
        }

        private Vector3 GetNextTarget
        {
            get
            {
                if((counter++ % 50 == 0))
                {
                    CheckBehaviourChange();
                    switch (currentBehaviour)
                    {
                        case Behaviours.Attack:
                            lastTarget = GetNextTargetAttack();
                            break;
                        case Behaviours.MidAttack:
                            lastTarget = GetNextTargetMidAttack();
                            break;
                        case Behaviours.Defend:
                            lastTarget = GetNextTargetDefend();
                            break;
                        case Behaviours.MidDefend:
                            lastTarget = GetNextTargetMidDefend();
                            break;
                        default:
                            lastTarget = transform.position;
                            break;
                    }
                }
                lastTarget.y = transform.position.y;
                return lastTarget;
            }
        }

        private void CheckBehaviourChange()
        {
            float gameBallDistanceToGoal = Mathf.Abs(gameBall.position.x - myGoal.x);
            if (gameBallDistanceToGoal<FieldSize.x/3)
            {
                switch (startingBehaviour)
                {
                    case Behaviours.MidDefend:
                        currentBehaviour = Behaviours.Defend;
                        break;
                    case Behaviours.MidAttack:
                        currentBehaviour = Behaviours.MidDefend;
                        break;
                }
                return;
            }
            if (gameBallDistanceToGoal < FieldSize.x*2 / 3)
            {
                currentBehaviour = startingBehaviour;
                return;
            }
            switch (startingBehaviour)
            {
                case Behaviours.MidDefend:
                    currentBehaviour = Behaviours.MidAttack;
                    break;
                case Behaviours.MidAttack:
                    currentBehaviour = Behaviours.Attack;
                    break;
            }
        }

        private Vector3 GetNextTargetAttack()
        {
            if (TryGoal.HasValue)
            {
                currentDecision = Decisions.ShootGoal;
                return TryGoal.Value;
            }
            if (MoveToAtkPosition.HasValue)
            {
                currentDecision = Decisions.MoveIntoAtkArea;
                return MoveToAtkPosition.Value;
            }
            if (RepositionBehindBall.HasValue)
            {
                currentDecision = Decisions.RepositionBehindBall;
                return RepositionBehindBall.Value;
            }
            currentDecision = Decisions.MoveToBall;
            return MoveToBall;
        }

        private Vector3 GetNextTargetMidAttack()
        {
            if (TryGoal.HasValue)
            {
                currentDecision = Decisions.ShootGoal;
                return TryGoal.Value;
            }
            if (MoveHomeConditional.HasValue)
            {
                currentDecision = Decisions.GoHomeConditional;
                return MoveHomeConditional.Value;
            }
            if (TrySave.HasValue)
            {
                currentDecision = Decisions.ShootBallAway;
                return TrySave.Value;
            }
            if (RepositionBehindBall.HasValue)
            {
                currentDecision = Decisions.RepositionBehindBall;
                return RepositionBehindBall.Value;
            }
            currentDecision = Decisions.GoHome;
            return MoveHome;
        }

        private Vector3 GetNextTargetDefend()
        {
            if (MoveToDefPosition.HasValue)
            {
                currentDecision = Decisions.MoveIntoDefArea;
                return MoveToDefPosition.Value;
            }
            currentDecision = Decisions.MoveToBall;
            return MoveToBall;
        }

        private Vector3 GetNextTargetMidDefend()
        {
            if (BetweenBall.HasValue)
            {
                currentDecision = Decisions.GetInBetweenBallAndGoal;
                return BetweenBall.Value;
            }
            if (TrySave.HasValue)
            {
                currentDecision = Decisions.ShootBallAway;
                return TrySave.Value;
            }
            if (RepositionBehindBall.HasValue)
            {
                currentDecision = Decisions.RepositionBehindBall;
                return RepositionBehindBall.Value;
            }
            if (MoveToBallConditional.HasValue)
            {
                currentDecision = Decisions.MoveToBallConditional;
                return MoveToBallConditional.Value;
            }
            
           currentDecision = Decisions.GoHome;
            return MoveHome;
        }

        private Vector3? TryGoal
        {
            get
            {
                float angle = GetAngleToEnemyGoal;
                if (angle > 175)
                {
                    return gameBall.position;
                }
                return null;
            }
        }

        private Vector3? TrySave
        {
            get
            {
                float angle = GetAngleToEnemyGoal;
                if (angle < 90)
                {
                    return gameBall.position;
                }
                return null;
            }
        }

        private Vector3? BetweenBall
        {
            get
            {
                float angle = GetAngleToMyGoal;
                if (angle >175)
                {
                    return gameBall.position;
                }
                return null;
            }
        }

        float GetMyDistance(Vector3 a, Vector3 b)
        {
            a.y = 0;
            b.y = 0;
            return Vector3.Distance(a, b);
        }

        private Vector3? MoveHomeConditional
        {
            get
            {
                if (GetMyDistance(transform.position, topAtkCorner.position) < FieldSize.y * 0.15f||
                    GetMyDistance(transform.position, bottomAtkCorner.position) < FieldSize.y * 0.15f)
                {
                    return MoveHome;
                }
                return null;
            }
        }

        private Vector3? MoveToBallConditional
        {
            get
            {
                Vector3 dirBotCornerToBall = (bottomDefCorner.position - gameBall.position).normalized;
                Vector3 dirBallToGoal = (myGoal - gameBall.position).normalized;
                Vector3 dirTopCornerToBall = (topDefCorner.position - gameBall.position).normalized;
                if (Vector3.AngleBetween(dirBotCornerToBall, dirBallToGoal) > 170 ||
                    Vector3.AngleBetween(dirTopCornerToBall, dirBallToGoal) > 170)
                {
                    return MoveToBall;
                }
                return null;
            }
        }

        private Vector3 MoveToBall
        {
            get
            {
                return gameBall.position;
            }
        }

        private Vector3? MoveToAtkPosition
        {
            get
            {
                float ballDistanceToTopAtkCorner = GetMyDistance(topAtkCorner.position, gameBall.position);
                float ballDistanceToBottomAtkCorner = GetMyDistance(bottomAtkCorner.position, gameBall.position);
                Vector3 atkPosition = ballDistanceToTopAtkCorner > ballDistanceToBottomAtkCorner ? bottomAtkCorner.position : topAtkCorner.position;
                if (GetMyDistance(transform.position, atkPosition) < FieldSize.y * (0.6f)) {
                    return null;
                }
                return atkPosition;  
            }
        }

        private Vector3? MoveToDefPosition
        {
            get
            {
                if (GetMyDistance(transform.position, myGoal) < FieldSize.y * (0.25f))
                {
                    return null;
                }
                return myGoal;
            }
        }

        private Vector3 MoveHome
        {
            get
            {
                if (GetMyDistance(transform.position, myGoal)<5)
                {
                    return transform.position;
                }
                return myGoal;
            }
        }

        private Vector3? RepositionBehindBall
        {
            get
            {
                if (GetMyDistance(transform.position, myGoal) < GetMyDistance(gameBall.position,myGoal))
                {
                    Vector3 pos = transform.position;
                    pos.z = UnityEngine.Random.RandomRange(gameBall.position.z-7.5f, gameBall.position.z +7.5f);
                    return pos;
                }
                return null;
            }
        }



        private float GetAngleToEnemyGoal
        {
            get
            {
                Vector3 shootDirection = (gameBall.position - transform.position).normalized;
                Vector3 goalDirection = (enemyGoal - gameBall.position).normalized;
                float angle = Vector3.Angle(shootDirection, goalDirection);
                return angle;
            }
        }

        private float GetAngleToMyGoal
        {
            get
            {
                Vector3 shootDirection = (gameBall.position - transform.position).normalized;
                Vector3 goalDirection = (gameBall.position - myGoal).normalized;
                float angle = Vector3.Angle(shootDirection, goalDirection);
                return angle;
            }
        }

        private Vector2 FieldSize
        {
            get
            {
                return new Vector2(GetMyDistance(topAtkCorner.position, topDefCorner.position),
                    GetMyDistance(topAtkCorner.position, bottomAtkCorner.position));
            }
        }

        public override bool Shot
        {
            get
            {
                return gameBall.position.y < 1.6f && currentDecision == Decisions.ShootBallAway;
            }
        }

        public override bool Punch
        {
            get
            {
                return false;
            }
        }

        public override bool Shot2
        {
            get
            {
                return gameBall.position.y < 1.6f && currentDecision == Decisions.ShootGoal;
            }
        }

        public Transform BottomAtkCorner
        {
            get { return bottomAtkCorner; }
            set { bottomAtkCorner = value; }
        }

        public Transform BottomDefCorner
        {
            get { return bottomDefCorner; }
            set { bottomDefCorner = value; }
        }

        public Transform TopAtkCorner
        {
            get { return topAtkCorner; }
            set { topAtkCorner = value; }
        }

        public Transform TopDefCorner
        {
            get { return topDefCorner; }
            set { topDefCorner = value; }
        }
    }
}
