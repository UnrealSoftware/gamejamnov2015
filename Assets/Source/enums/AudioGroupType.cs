﻿namespace Assets.Source.enums
{
    public enum AudioGroupType
    {
        Music,
        Ambience,
        Sound
    }
}
