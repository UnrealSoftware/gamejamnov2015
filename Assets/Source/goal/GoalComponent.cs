﻿using UnityEngine;
using Gameplay;

namespace Assets.Goal
{    
    public class GoalComponent : MonoBehaviour
    {
        public int TeamID;

        [SerializeField] private ParticleSystem leftEyeParticle;
        [SerializeField] private ParticleSystem rightEyeParticle;
        [SerializeField] private ParticleSystem fireBreathParticle;

        public void Start()
        {
            leftEyeParticle.startColor = TeamID == 1 ? Color.red : Color.blue;
            rightEyeParticle.startColor = TeamID == 1 ? Color.red : Color.blue;
        }

        public void OnTriggerExit(Collider other)
        {
            var ball = other.GetComponent<BallPhysics>();

            if (ball)
            {
                // This is the ID of the goal but we need to pass the one of the team
                // which gets a point
                GameManager.Instance.ScoreTeam((TeamID == 1) ? 2 : 1);

                // play fire breath
                fireBreathParticle.Play();
            }
        }
    }
}