﻿using System.Collections;
using DG.Tweening;
using UnityEngine;

namespace Assets.Source.arenaObjects
{
    public class HolyStone : MonoBehaviour
    {
        [SerializeField] private float duration = 1;
        [SerializeField] private float minRandomTime = 1f;
        [SerializeField] private float maxRandomTime = 1f;
        private bool isActive;

        private Vector3 tempPosition;

        private Tween tween;

        private AudioSource AudioSource { get; set; }

        protected void Awake()
        {
            AudioSource = GetComponent<AudioSource>();
        }

        protected void Start()
        {
            tempPosition = transform.position;
            SetObjectActive();
        }

        private void SetObjectActive()
        {
            isActive = true;
            if (AudioSource && !AudioSource.isPlaying)
            {
                AudioSource.Play();
            }
            StartCoroutine(WaitForStartMoveDown(Random.Range(minRandomTime,maxRandomTime)));

        }

        private void SetObjectInActive()
        {
            isActive = false;
            if (AudioSource && AudioSource.isPlaying)
            {
                AudioSource.Stop();
            }
            StartCoroutine(WaitForStartMoveUp(Random.Range(minRandomTime,maxRandomTime)));
        }

        private IEnumerator WaitForStartMoveUp(float time)
        {
            yield return new WaitForSeconds(time);
            if (tween != null && tween.IsInitialized())
            {

                tween.Kill();
            }
            tween = transform.DOMoveY(tempPosition.y + 8, duration).OnComplete(SetObjectActive);
        }

        private IEnumerator WaitForStartMoveDown(float time)
        {
            yield return new WaitForSeconds(time);
            if (tween != null && tween.IsInitialized())
            {

                tween.Kill();
            }
            tween = transform.DOMoveY(tempPosition.y, duration).OnComplete(SetObjectInActive);
        }
    }
}
