﻿using Assets.Source.Delegates;
using Assets.Source.PlayerController;
using Assets.Source.Root;
using UnityEngine;

namespace Assets.Source.menu
{
    [RequireComponent(typeof(PlayerInput))]

    public class HelpScreen : MonoBehaviour
    {
        private PlayerInput input;
        
        protected void Awake()
        {
            input = GetComponent<PlayerInput>();
        }

        protected void Start()
        {
        }

        protected void Update()
        {
            if (input.ButtonBDown)
            {
                BackToMainMenu();
            }
        }

        private void BackToMainMenu()
        {
            SceneEvent.ChangeScene(SceneType.MainMenu);
        }
    }
}
