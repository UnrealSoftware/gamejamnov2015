﻿using Assets.Source.Delegates;
using Assets.Source.PlayerController;
using Assets.Source.Root;
using DG.Tweening;
using UnityEngine;

namespace Assets.Source.menu
{
    [RequireComponent(typeof(PlayerInput))]
    public class Credits : MonoBehaviour
    {
        private PlayerInput input;

        [SerializeField] private float duration = 5f;
        [SerializeField] private float offset = 200f;
        protected void Awake()
        {
            input = GetComponent<PlayerInput>();
        }

        protected void Start()
        {
            transform.DOMoveY(transform.position.y + offset, duration).OnComplete(BackToMainMenu).SetEase(Ease.Linear);
        }

        protected void Update()
        {
            if (input.ButtonBDown)
            {
               BackToMainMenu();
            }
        }

        private void BackToMainMenu()
        {
            SceneEvent.ChangeScene(SceneType.MainMenu);
        }
    }
}
