﻿using System;
using Assets.Source.Delegates;
using Assets.Source.PlayerController;
using Assets.Source.Root;
using Assets.Source.Root.Scripts.Roots;
using DG.Tweening;
using UnityEngine;

namespace Assets.Source.menu
{
    [RequireComponent(typeof(PlayerInput))]
    public class MainMenu : MonoBehaviour
    {
        [SerializeField] private RotateMode rotateMode = RotateMode.Fast;
        [SerializeField] private float duration = 1f;
        [SerializeField] private float shake = 5f;
        [SerializeField] private TextMesh textAmountOfPlayer;
        [SerializeField] private TextMesh textPlayTime;

        private PlayerInput input;
        private Tween tween;
        public SceneType scene;
        private bool inMenuPlayTime = false;
        private bool inMenuAmountPlayer = false;
        protected void Awake()
        {
            input = GetComponent<PlayerInput>();
        }

        protected void Start()
        {
            input = GetComponent<PlayerInput>();
            scene = SceneType.Arena;
            if (!GameRoot.Instance.FirstStart)
            {
                transform.position = Vector3.up * 10;
                tween = transform.DOMove(Vector3.zero, 6).OnComplete(OnCompleteMovement);
                GameRoot.Instance.FirstStart = true;
            }
           
            FirstPlayerAmountCheck();
            UpdateTextAmountOfPlayers(GameRoot.Instance.PlayerNumbers);
            UpdateTimeForPlay(GameRoot.Instance.PlayTime);
        }

        private static void FirstPlayerAmountCheck()
        {
            PlayerInput[] players = FindObjectsOfType<PlayerInput>();
            int amount = 0;
            for (int i = 0; i < players.Length; i++)
            {
                if (players[i].IsActive)
                {
                    amount ++;
                }
            }
            GameRoot.Instance.PlayerNumbers = Mathf.Max(1, amount);
        }

        protected void Update()
        {
            Vector3 rotation = transform.eulerAngles;
            //choose the wrong stuff x == y and y == x
            rotation.y += input.LeftHorizontal;
            rotation.x += input.LeftVertical;
            if (input.LeftHorizontal > 0 && (tween == null || !tween.IsPlaying()))
            {
                if (rotation.y > 0 && rotation.y < 90)
                {
                    tween = transform.DORotate(Vector3.up*90, duration, rotateMode).OnComplete(OnCompleteRotation);
                    scene = SceneType.Credits;
                }
                if (rotation.y > 90 && rotation.y < 180)
                {
                    tween = transform.DORotate(Vector3.up * 180, duration, rotateMode).OnComplete(OnCompleteRotation);
                    scene = SceneType.Quit;
                }
                if (rotation.y > 180 && rotation.y < 270)
                {
                    tween = transform.DORotate(Vector3.up * 270, duration, rotateMode).OnComplete(OnCompleteRotation);
                    scene = SceneType.Highscore;
                }
                if (rotation.y > 270 && rotation.y < 360)
                {
                    tween = transform.DORotate(Vector3.up * 360, duration, rotateMode).OnComplete(OnCompleteRotation);
                    scene = SceneType.Arena;
                }
            }
            if (input.LeftHorizontal < 0 && (tween == null || !tween.IsPlaying()))
            {
                if (rotation.y < 360 && (rotation.y > 270 || rotation.y < 1))
                {
                    tween = transform.DORotate(Vector3.up * 270, duration, rotateMode).OnComplete(OnCompleteRotation);
                    scene = SceneType.Highscore;
                }
                if (rotation.y < 270 && rotation.y > 180)
                {
                    tween = transform.DORotate(Vector3.up * 180, duration, rotateMode).OnComplete(OnCompleteRotation);
                    scene = SceneType.Quit;
                }
                if (rotation.y < 180 && rotation.y > 90)
                {
                    tween = transform.DORotate(Vector3.up * 90, duration, rotateMode).OnComplete(OnCompleteRotation);
                    scene = SceneType.Credits;
                }
                if (rotation.y < 90 && rotation.y > 0)
                {
                    tween = transform.DORotate(Vector3.up * 360, duration, rotateMode).OnComplete(OnCompleteRotation);
                    scene = SceneType.Arena;
                }
            }
            if (input.LeftVertical > 0 && (tween == null || !tween.IsPlaying()))
            {
                if (rotation.y < 90 && rotation.y >= 0)
                {
                    tween = transform.DORotate(Vector3.left * 90, duration, rotateMode).OnComplete(OnCompleteRotation);
                    inMenuAmountPlayer = true;
                    scene = SceneType.None;
                }
                if (rotation.x >= 90)
                {
                    tween = transform.DORotate(Vector3.left * 0, duration, rotateMode).OnComplete(OnCompleteRotation);
                    inMenuPlayTime = false;
                    scene = SceneType.Arena;
                }
            }
            if (input.LeftVertical < 0 && (tween == null || !tween.IsPlaying()))
            {
              

                if (rotation.x > 0 && rotation.x <= 270)
                {
                    tween = transform.DORotate(Vector3.right * 0 , duration, rotateMode).OnComplete(OnCompleteRotation);
                    scene = SceneType.Arena;
                    inMenuAmountPlayer = false;
                }
                else if (rotation.y < 90 && rotation.y >= 0)
                {
                    tween = transform.DORotate(Vector3.right * 90, duration, rotateMode).OnComplete(OnCompleteRotation);
                    inMenuPlayTime = true;
                    scene = SceneType.None;
                }
            }

            if (input.ButtonADown)
            {
                if (scene != SceneType.None)
                {
                    SceneEvent.ChangeScene(scene);
                }
                else if(inMenuAmountPlayer)
                {
                    int amount = GameRoot.Instance.PlayerNumbers;
                    amount++;
                    if (amount > 4)
                    {
                        amount = 1;
                    }
                    GameRoot.Instance.PlayerNumbers = amount;
                    UpdateTextAmountOfPlayers(amount);
                }
                else if (inMenuPlayTime)
                {
                    int time = GameRoot.Instance.PlayTime;
                    time+= 10;
                    if (time > 600)
                    {
                        time = 10;
                    }
                    GameRoot.Instance.PlayTime = time;
                    UpdateTimeForPlay(time);
                }
            }
            else if (input.ButtonBDown)
            {
                if (inMenuAmountPlayer)
                {
                    int amount = GameRoot.Instance.PlayerNumbers;
                    amount--;
                    if (amount < 1)
                    {
                        amount = 4;
                    }
                    GameRoot.Instance.PlayerNumbers = amount;
                    UpdateTextAmountOfPlayers(amount);
                }
                if (inMenuPlayTime)
                {
                    int time = GameRoot.Instance.PlayTime;
                    time -= 10;
                    if (time < 10)
                    {
                        time = 600;
                    }
                    GameRoot.Instance.PlayTime = time;
                    UpdateTimeForPlay(time);
                }
            }
        }

        private void OnCompleteRotation()
        {
           
        }

         public void OnCompleteMovement()
         {
           
         }

        private void UpdateTextAmountOfPlayers(int amount)
        {
            textAmountOfPlayer.text = "Player\nAmount\n" + amount;
        }

        private void UpdateTimeForPlay(int time)
        {
            TimeSpan t = TimeSpan.FromSeconds(time);
            textPlayTime.text = "Playtime\n" + string.Format("{0:D2}:{1:D2}", t.Minutes, t.Seconds);            
        }
    }
}
