﻿using UnityEngine;
using UnityEngine.UI;
using Gameplay;

namespace UI
{ 
    public class ScoreBoardComponent : MonoBehaviour {

        [SerializeField]
        private Text scoreTeam1Text;

        [SerializeField]
        private Text scoreTeam2Text;

        [SerializeField]
        private Text team1GoalText;

        [SerializeField]
        private Text team2GoalText;

        [SerializeField]
        private Text team1WinText;

        [SerializeField]
        private Text team2WinText;

        [SerializeField]
        private Text drawText;


        public void Start() {
            team1GoalText.gameObject.SetActive(false);
            team2GoalText.gameObject.SetActive(false);
            team1WinText.gameObject.SetActive(false);
            team2WinText.gameObject.SetActive(false);
            drawText.gameObject.SetActive(false);
        }

        public void Update()
        {
            scoreTeam1Text.text = GameManager.Instance.ScoreTeam1.ToString();

            scoreTeam2Text.text = GameManager.Instance.ScoreTeam2.ToString();
        }

        public void UpdateGoalText(int team) {
            team1GoalText.gameObject.SetActive(team == 1);
            team2GoalText.gameObject.SetActive(team == 2);
        }

        public void ShowGameOverText() {
            int score1 = GameManager.Instance.ScoreTeam1;
            int score2 = GameManager.Instance.ScoreTeam2;
            team1WinText.gameObject.SetActive(score1 > score2);
            team2WinText.gameObject.SetActive(score2 > score1);
            drawText.gameObject.SetActive(score1 == score2);
        }
    }
}
