﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TimeComponent : MonoBehaviour {

    public int StartTime = 60;

    [SerializeField]
    private Text timeText;

    [Header("Slow Flash")]
    [SerializeField]
    private int slowFlashThresholdSeconds = 10; 

    [SerializeField]
    private float slowFlashMultiplier = 5f;

    [Header("Fast Flash")]
    [SerializeField]
    private int fastFlashThresholdSeconds = 3;

    [SerializeField]
    private float fastFlashMultiplier = 10f;

    private bool gameRunning = true;
    private int seconds;
    public int Seconds {
        get { return seconds; }
        set {
            seconds = value;

            TimeSpan t = TimeSpan.FromSeconds(seconds);
            timeText.text = string.Format("{0:D2}:{1:D2}", t.Minutes, t.Seconds);
        }
    }
    
    // Use this for initialization
    void Start () {
        Seconds = StartTime;
        StartCoroutine(CountDown());
    }
	
	// Update is called once per frame
	void Update () {
	    if (seconds < slowFlashThresholdSeconds) {
	        float flashFactor = (seconds < fastFlashThresholdSeconds) ? fastFlashMultiplier : slowFlashMultiplier;
            float a = Mathf.Abs(Mathf.Sin(Time.time * flashFactor));
            timeText.color = new Color(1f, a, a, 1f);
	    }
	    else {
	        timeText.color = Color.white;
	    }
	}

    private IEnumerator CountDown() {
        while (true) {
            if (Seconds > 0 && gameRunning) {
                Seconds--;
            }
            yield return new WaitForSeconds(1);
        }
    }

    public void Pause()
    {
        gameRunning = false;
    }

    public void Resume()
    {
        gameRunning = true;
    }
}
