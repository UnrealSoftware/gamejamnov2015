﻿using System;
using UnityEngine;
using System.Collections;

public class LeafShaker : MonoBehaviour {

    public float ShakeSpeed = 2f;

    public float ShakePower = 2f;

    private RectTransform rectTransform;

    private float startRotation;

	// Use this for initialization
	void Start () {
	    rectTransform = GetComponent<RectTransform>();
	    startRotation = rectTransform.rotation.eulerAngles.z;
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 rot = rectTransform.rotation.eulerAngles;
	    rot.z = startRotation + Mathf.Sin(Time.time * ShakeSpeed) * ShakePower;
	    rectTransform.localEulerAngles = rot;
	}
}
