﻿using Assets.Source.items;
using UnityEngine;

namespace Gameplay
{
    public class Item : MonoBehaviour
    {
        public float LifeTime = 5;
        public ItemType Type = ItemType.Coin;
                
        public void Update()
        {
            LifeTime -= Time.deltaTime;
        }

        public void LateUpdate()
        {
            if (LifeTime <= 0)
            {
                Destroy(gameObject);
            }
        }

        // Update is called once per frame
        public void FixedUpdate()
        {
            var rotation = new Vector3(0, 60, 0);

            transform.Rotate(rotation * Time.deltaTime);
        }

        public void OnTriggerEnter(Collider other)
        {
            var player = other.GetComponent<PlayerMovement>();

            if (player)
            {
                player.CollectItem(Type);

                Destroy(gameObject);
            }
        }
    }
}