﻿using UnityEngine;

namespace Gameplay
{
    public class ItemSpawner : MonoBehaviour
    {
        public Item[] ItemPrefabs;
        public float SpawnTime = 5;

        private float lastSpawend;

        private System.Random random;

        // Use this for initialization
        void Start()
        {
            random = new System.Random();
        }

        // Update is called once per frame
        void Update()
        {
            if (Time.time - lastSpawend > SpawnTime)
            {
                Spawn();
            }
        }

        private void Spawn()
        {
            var index = random.Next(0, ItemPrefabs.Length);

            var go = Instantiate(ItemPrefabs[index]);

            var x = random.Next(-15,15);
            var y = random.Next(-15,15);

            go.transform.position = new Vector3(x,2,y);

            lastSpawend = Time.time;
        }
    }
}