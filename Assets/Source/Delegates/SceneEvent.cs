﻿using Assets.Source.Root;

namespace Assets.Source.Delegates
{
    public static class SceneEvent
    {
        public delegate void ChangeSceneEventHandler(SceneType type);

        public static ChangeSceneEventHandler ChangeScene;
    }
}
