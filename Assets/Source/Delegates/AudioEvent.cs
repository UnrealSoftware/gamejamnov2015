﻿using Assets.Source.enums;
using Assets.Source.Root;
using UnityEngine;

namespace Assets.Source.Delegates
{
    public static class AudioEvent
    {
        public delegate void ChangeAudioEventHandler(AudioGroupType group,AudioFileType file);

        public static ChangeAudioEventHandler ChangeAudio;
    }
}
