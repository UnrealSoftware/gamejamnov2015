﻿namespace Assets.Source.Root
{
    public enum SceneType
    {
        None,
        Init,
        MainMenu,
        Arena,
        Highscore,
        Credits,
        Quit
    }
}
