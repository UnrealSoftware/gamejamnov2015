﻿using Assets.Source.Delegates;
using Assets.Source.enums;

namespace Assets.Source.Root
{
    using UnityEngine;

    namespace Scripts.Roots
    {
        public class GameRoot : MonoBehaviour
        {
            public static GameRoot Instance { get; private set; }

            [SerializeField] private AudioSource music;
            [SerializeField] private AudioSource sound;
            [SerializeField] private AudioSource ambience;
            
            
            public int PlayerNumbers { get; set; }
            public int PlayTime { get; set; }

            public bool FirstStart { get; set; }

            protected  void Awake()
            {
                if (Instance != null && Instance != this)
                {
                    Destroy(gameObject);
                }

                Instance = this;
                
                DontDestroyOnLoad(gameObject);

            }

            protected void Start()
            {
                PlayTime = 60;
                PlayerNumbers = 1;
                OnChangeScene(SceneType.MainMenu);
                OnChangeAudio(AudioGroupType.Music, AudioFileType.PitziMusic);
            }

            protected void OnEnable()
            {
                SceneEvent.ChangeScene += OnChangeScene;
                AudioEvent.ChangeAudio += OnChangeAudio;
            }

            protected void OnDisable()
            {
                SceneEvent.ChangeScene -= OnChangeScene;
            }

            private void OnChangeScene(SceneType scene)
            {
                if (scene == SceneType.None)
                {
                    Debug.LogWarning("No scene is selecetd");
                    return;
                }
                if (scene != SceneType.Quit)
                {
                    Application.LoadLevel(scene.ToString());
                }
                else
                {
                    Application.Quit();
                }
            }

            private void OnChangeAudio(AudioGroupType group, AudioFileType file)
            {
                string path = "Audio/" + group + "/" + file;
                switch (group)
                {
                    case AudioGroupType.Music:
                       music.clip = Resources.Load<AudioClip>(path);
                        music.Play();
                        break;
                    case AudioGroupType.Sound:
                        sound.clip = Resources.Load<AudioClip>(path);
                        sound.Play();
                        break;
                    case AudioGroupType.Ambience:
                        ambience.clip = Resources.Load<AudioClip>(path);
                       ambience.Play();
                        break;
                }
            }

        }
    }

}
