﻿using UnityEngine;

namespace Assets
{
    public class MovingPlatforms : MonoBehaviour
    {
        [SerializeField]
        private GameObject platform;

        [SerializeField]
        private int scale;

        [SerializeField]
        private int offset;

        private GameObject[,] platforms;

        // Use this for initialization
        public void Start()
        {
            platforms = new GameObject[3, 3];

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    var go = Instantiate(platform);
                    go.SetActive(true);

                    go.transform.position = new Vector3(-scale + (i * scale), -(offset + (scale / 2)), -scale + (j * scale));
                    go.transform.localScale = new Vector3(scale, scale, scale);

                    go.transform.SetParent(transform, true);

                    platforms[i, j] = go;
                }
            }
        }

        // Update is called once per frame
        public void FixedUpdate()
        {
            var translate = Mathf.Sin(Time.time * Time.deltaTime * Mathf.PI * 2) * scale;
            var translate2 = (Mathf.Sin((Time.time * Time.deltaTime * Mathf.PI * 2) + Mathf.PI) * scale);

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    var oldPosition = platforms[i, j].transform.position;

                    var iOdd = i % 2 == 1;
                    var jOdd = j % 2 == 1;

                    if ((iOdd && jOdd) || (!iOdd && !jOdd))
                    {
                        platforms[i, j].transform.position = new Vector3(oldPosition.x, translate - (offset + (scale / 2)), oldPosition.z);
                    }
                    else
                    {
                        platforms[i, j].transform.position = new Vector3(oldPosition.x, translate2 - (offset + (scale / 2)), oldPosition.z);
                    }


                }
            }
        }
    }
}